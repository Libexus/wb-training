"use strict"
/* © Libexus, 2021 */

let mainElement = document.getElementById("main-elem");

let labelA = document.getElementById("letterA");
let labelB = document.getElementById("letterB");
labelB.timeout = sleep(0);

let locked = false;

let button_left = document.getElementById("left");
let button_right = document.getElementById("right");

let stats_correct = document.getElementById("stats-correct");
let stats_incorrect = document.getElementById("stats-incorrect");
let stats_total = document.getElementById("stats-total");

let pin = document.getElementById("pin");
let pinned = false;

const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
let letterA;
let letterB;
let letterA_i;
let letterB_i;

updateLetters();

labelB.addEventListener("transitionstart", () => locked = true);
labelB.addEventListener("transitioncancel", () => locked = false);
labelB.addEventListener("transitionend", async() => {
    await labelB.timeout; // Animation fertig UND Mindestzeit vergangen
    updateLetters();
    labelB.classList.remove("to-left");
    labelB.classList.remove("to-right");
    locked = false;
});

button_left.onclick = clickLeft;
button_right.onclick = clickRight;

pin.addEventListener("click", () => {
    if(pinned = !pinned) {
        pin.classList.add("pinned");
    } else {
        pin.classList.remove("pinned");
    }
});

document.body.onkeypress = (e) => {
    if(e.key == "j") {
        clickLeft();
    } else if (e.key == "k") {
        clickRight();
    }
}

function updateLetters() {
    if(!pinned) {
        letterA_i = Math.floor((Math.random() * letters.length));
        letterA = letters[letterA_i];
    }
    do {
        letterB_i = Math.floor((Math.random() * letters.length));
        letterB = letters[letterB_i];
    } while (letterB == letterA);
    labelA.textContent = letterA;
    labelB.textContent = letterB;
}

function clickLeft() {
    if(!locked && check(false)) {
        labelB.classList.add("to-left");
        labelB.timeout = sleep(1000);
    }
}

function clickRight() {
    if(!locked && check(true)) {
        labelB.classList.add("to-right");
        labelB.timeout = sleep(1000);
    }
}

function check(toright) {
    if(letterA_i < letterB_i == toright) {
        stats_correct.textContent = Number(stats_correct.textContent) + 1;
        stats_total.textContent = Number(stats_total.textContent) + 1;
        triggerAnimation("greenBlink", mainElement);
        return true;
    } else {
        stats_incorrect.textContent = Number(stats_incorrect.textContent) + 1;
        stats_total.textContent = Number(stats_total.textContent) + 1;
        triggerAnimation("redBlink", mainElement);
        return false;
    }
}

function triggerAnimation(name, node) {
    node.addEventListener("animationend", () => node.classList.remove(name), {once: true});
    node.classList.add(name);
}

async function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
